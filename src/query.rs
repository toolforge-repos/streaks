// SPDX-License-Identifier: AGPL-3.0-or-later
// (C) Copyright 2023 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use mwseaql::sea_query::{Expr, MysqlQueryBuilder, Query};
use mwseaql::{ActorRevision, Page, RevisionUserindex};
use toolforge::pool::mysql_async::{prelude::*, Conn};

pub(crate) async fn dates(
    conn: &mut Conn,
    username: &str,
    articles: bool,
) -> Result<Vec<String>> {
    let query = {
        let mut query = Query::select();
        query
            .distinct()
            .expr(Expr::cust("SUBSTRING(`rev_timestamp`, 1, 8)"))
            .from(RevisionUserindex::Table)
            .inner_join(
                ActorRevision::Table,
                Expr::col(RevisionUserindex::Actor).equals(ActorRevision::Id),
            )
            .and_where(Expr::col(ActorRevision::Name).eq(username));
        if articles {
            query
                .inner_join(
                    Page::Table,
                    Expr::col(RevisionUserindex::Page).equals(Page::Id),
                )
                .and_where(Expr::col(Page::Namespace).eq(0));
        }
        query.to_string(MysqlQueryBuilder)
    };
    Ok(conn.query(query).await?)
}
