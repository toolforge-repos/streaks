// SPDX-License-Identifier: AGPL-3.0-or-later
// (C) Copyright 2023 Kunal Mehta <legoktm@debian.org>
mod query;

#[macro_use]
extern crate rocket;

use cached::proc_macro::once;
use chrono::{Duration, NaiveDate, Utc};
use mwtitle::TitleCodec;
use rocket::http::ContentType;
use rocket::State;
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::Serialize;
use std::collections::BTreeSet;
use toolforge::pool::{WikiPool, WEB_CLUSTER};

const HUMAN_DATE: &str = "%B %-d, %Y";

/// The context needed to render the "index.html" template
#[derive(Serialize)]
struct IndexTemplate {
    wikis: BTreeSet<String>,
}

#[derive(Serialize)]
struct ErrorTemplate {
    code: u16,
    reason: &'static str,
    error: Option<String>,
}

#[derive(Responder)]
#[response(status = 500, content_type = "text/html")]
struct Error(Template);

type Result<T> = std::result::Result<T, Error>;

impl From<anyhow::Error> for Error {
    fn from(value: anyhow::Error) -> Self {
        Self(Template::render(
            "error",
            ErrorTemplate {
                code: 500,
                reason: "Internal Server Error",
                error: Some(value.to_string()),
            },
        ))
    }
}

#[once(sync_writes = true, result = true, time = 86400)]
async fn all_wikis(pool: &WikiPool) -> anyhow::Result<BTreeSet<String>> {
    Ok(pool.list_domains().await?)
}

/// Handle all GET requests for the "/" route. We return either a `Template`
/// instance, or a `Template` instance with a specific HTTP status code.
///
/// We ask Rocket to give us the managed State (see <https://rocket.rs/v0.5-rc/guide/state/#managed-state>)
/// of the database connection pool we set up below.
#[get("/")]
async fn index(pool: &State<WikiPool>) -> Result<Template> {
    let wikis = all_wikis(pool).await?;
    Ok(Template::render("index", IndexTemplate { wikis }))
}

#[get("/lookup?<username>&<domain>&<articles>")]
async fn lookup_route(
    pool: &State<WikiPool>,
    codec: &State<TitleCodec>,
    username: String,
    domain: String,
    articles: Option<String>,
) -> Result<Template> {
    let username = normalize_username(codec, username);
    Ok(lookup(pool, username, domain, articles.is_some()).await?)
}

#[derive(Serialize)]
struct LookupTemplate {
    username: String,
    domain: String,
    articles: bool,
    longest: Option<FormattedStreak>,
    active: Option<FormattedStreak>,
    streaks: Vec<FormattedStreak>,
    wikis: BTreeSet<String>,
}

async fn lookup(
    pool: &WikiPool,
    username: String,
    domain: String,
    articles: bool,
) -> anyhow::Result<Template> {
    let mut conn = pool.connect_by_domain(&domain).await?;
    let edits = query::dates(&mut conn, &username, articles).await?;
    let streaks = calculate_streaks(edits);
    let longest = longest_streak(&streaks);
    // TODO: should we look at yesterday rather than today? Will "break" streaks at midnight then
    let active = if !streaks.is_empty()
        && streaks[0].finish == Utc::now().date_naive()
    {
        Some(streaks[0])
    } else {
        None
    };
    let streaks = streaks.into_iter().map(|x| x.into()).collect();
    let wikis = all_wikis(pool).await?;
    Ok(Template::render(
        "lookup",
        LookupTemplate {
            username,
            domain,
            articles,
            active: active.map(|x| x.into()),
            longest: longest.map(|x| x.into()),
            streaks,
            wikis,
        },
    ))
}

#[derive(Serialize)]
struct FormattedStreak {
    start: String,
    finish: String,
    length: u64,
}

impl From<Streak> for FormattedStreak {
    fn from(value: Streak) -> Self {
        Self {
            start: value.human_start(),
            finish: value.human_finish(),
            length: value.length,
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct Streak {
    finish: NaiveDate,
    length: u64,
}

impl Streak {
    fn start(&self) -> NaiveDate {
        self.finish - Duration::try_days(self.length as i64 - 1).unwrap()
    }

    fn human_start(&self) -> String {
        self.start().format(HUMAN_DATE).to_string()
    }

    fn human_finish(&self) -> String {
        self.finish.format(HUMAN_DATE).to_string()
    }
}

fn calculate_streaks(mut raw: Vec<String>) -> Vec<Streak> {
    raw.sort();
    raw.reverse();
    let mut streaks = vec![];
    let mut last = Utc::now().date_naive();
    let mut active = Streak {
        finish: last,
        length: 0,
    };
    for day in raw {
        let day = db_to_date(&day);
        if (last - Duration::try_days(1).unwrap()) != day {
            // Gap of more than one day, reset the streak
            if active.length >= 5 {
                // Only count it as a streak if it's >= 5 days
                streaks.push(active);
            }
            active = Streak {
                finish: day,
                length: 1,
            }
        } else {
            active.length += 1;
        }
        last = day;
    }
    if active.length >= 5 {
        streaks.push(active);
    }
    streaks
}

fn longest_streak(streaks: &[Streak]) -> Option<Streak> {
    let mut longest = 0;
    let mut streak = None;
    for check in streaks {
        if check.length > longest {
            longest = check.length;
            streak = Some(*check);
        }
    }
    streak
}

/// 20230119 -> date!(2023 - 01 - 19)
/// panic on non-well formed input
fn db_to_date(db: &str) -> NaiveDate {
    NaiveDate::from_ymd_opt(
        (db[0..=3]).parse().expect("invalid year"),
        (db[4..=5]).parse::<u32>().expect("invalid month"),
        (db[6..=7]).parse().expect("invalid day"),
    )
    .expect("invalid date")
}

fn normalize_username(codec: &TitleCodec, username: String) -> String {
    match codec.new_title(&username) {
        Ok(title) => title.dbkey().replace('_', " "),
        Err(_) => username,
    }
}

#[get("/static/styles.css")]
fn styles() -> (ContentType, &'static str) {
    (ContentType::CSS, include_str!("../static/styles.css"))
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        // Create a new database connection pool. We intentionally do not use
        // Rocket's connection pooling facilities as Toolforge policy requires that
        // we do not hold connections open while not in use. The Toolforge crate builds
        // a connection string that configures connection pooling in a way that doesn't
        // leave unused idle connections hanging around.
        .manage(WikiPool::new(WEB_CLUSTER).expect("unable to load db config"))
        .manage(
            TitleCodec::from_json(include_str!("../siteinfo.json"))
                .expect("failed to build codec"),
        )
        .mount("/", routes![index, lookup_route, styles])
        .attach(Template::fairing())
        .attach(Healthz::fairing())
}

#[test]
fn test_db_to_date() {
    assert_eq!(
        db_to_date("20230119"),
        NaiveDate::from_ymd_opt(2023, 1, 19).unwrap()
    );
}
